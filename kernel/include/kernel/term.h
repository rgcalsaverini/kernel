#ifndef KERNEL_TERM_H
#define KERNEL_TERM_H

#include <stddef.h>
#include <stdint.h>

#define COL_BLACK 0x0
#define COL_BLUE 0x1
#define COL_GREEN 0x2
#define COL_CYAN 0x3
#define COL_RED 0x4
#define COL_MAGENTA 0x5
#define COL_BROWN 0x6
#define COL_L_GRAY 0x7
#define COL_D_GRAY 0x8
#define COL_L_BLUE 0x9
#define COL_L_GREEN 0xa
#define COL_L_CYAN 0xb
#define COL_L_RED 0xc
#define COL_PINK 0xd
#define COL_YELLOW 0xe
#define COL_WHITE 0xf

/**
 * Initializes the terminal by clearing it
 */
void terminal_init();

/**
 * Returns the value to be put on the VGA buffer for one specific character
 */
uint16_t terminal_char(char c);

/**
 * Places a single character on the VGA buffer
 * @param c the character
 */
void terminal_put_c(char c);

/**
 * Print a string onto the terminal
 * @param str a null terminated string
 */
void terminal_print(const char *str);

/**
 * Scrolls the terminal down n lines
 * @param lines the positive number of lines to scroll down
 */
void terminal_scroll_down(uint16_t lines);

/**
 * Logs a line
 * @param str
 */
void terminal_log(const char *str, const char *level, uint8_t color);

/**
 * Logs an info line
 * @param str
 */
void terminal_log_info(const char *str);

/**
 * Logs an debug line
 * @param str
 */
void terminal_log_debug(const char *str);

/**
 * Logs an warning line
 * @param str
 */
void terminal_log_warning(const char *str);

/**
 * Logs an error line
 * @param str
 */
void terminal_log_error(const char *str);

/**
 * Logs an critical line
 * @param str
 */
void terminal_log_critical(const char *str);

/**
 * Sets the foreground color
 * @param color
 */
void terminal_set_fg_color(uint8_t color);

/**
 * Sets the background color
 * @param color
 */
void terminal_set_bg_color(uint8_t color);

/**
 * Sets the blinking. We fancy.
 * @param blink 0 to turn off, anything else to turn on
 */
void terminal_set_blink(uint8_t blink);

#endif //KERNEL_TERM_H
