// GCC provides these automatically
#include <stddef.h>
#include <stdint.h>
#include "../include/kernel/term.h"

// Make sure that I am compiling on the cross compiler
#if defined(__linux__)
#error "This code must be compiled with a cross-compiler"
#elif !defined(__i386__)
#error "This code must be compiled with an x86-elf compiler"
#endif

/**
 * The kernel's main entry point
 */
void kernel_main() {
    terminal_init();
    terminal_log_info("Starting up");
//    register int protected_mode asm ("r32");
//    if (protected_mode) terminal_log_warning("Running in protected mode");
//    else terminal_log_warning("Kernel is not running in protected mode");
//    terminal_log_debug("Testing interrupts table");
//    terminal_log_warning("Interrupt was not set");
//    terminal_log_error("Failed to capture keyboard input");
//    terminal_log_critical("Exiting");

}