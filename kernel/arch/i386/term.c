// GCC provides these automatically
#include "../../include/kernel/term.h"

#define TERM_IDX(ROW, COL) ((VGA_COLS * ROW) + COL)


// VGA buffer memory location and dimensions
volatile uint16_t *vga_buffer = (uint16_t *) 0xB8000;
const int VGA_COLS = 80;
const int VGA_ROWS = 25;

int cursor_col = 0;
int cursor_row = 0;
uint8_t fg_color = COL_WHITE;
uint8_t bg_color = COL_BLACK;
uint8_t blinking = 0x00;

void terminal_init() {
    for (int col = 0; col < VGA_COLS; col++) {
        for (int row = 0; row < VGA_ROWS; row++) {
            vga_buffer[TERM_IDX(row, col)] = terminal_char(' ');
        }
    }
}

uint16_t terminal_char(char c) {

    return (
            ((uint16_t) blinking << 15) |
            ((uint16_t) bg_color << 12) |
            ((uint16_t) fg_color << 8) |
            c
    );
}

void terminal_put_c(char c) {
    if (c == '\n') {
        cursor_col = 0;
        cursor_row++;
    } else {
        vga_buffer[TERM_IDX(cursor_row, cursor_col)] = terminal_char(c);
        cursor_col++;
    }

    if (cursor_col >= VGA_COLS) {
        cursor_col = 0;
        cursor_row++;
    }

    if (cursor_row >= VGA_ROWS) {
        terminal_scroll_down(1);
        cursor_col = 0;
        cursor_row = VGA_ROWS - 1;
    }
}

void terminal_print(const char *str) {
    for (size_t i = 0; str[i] != '\0'; i++) terminal_put_c(str[i]);
}

void terminal_scroll_down(uint16_t lines) {
    int offset = VGA_COLS * lines;
    int until = VGA_COLS * (VGA_ROWS - lines);
    int last = VGA_COLS * VGA_ROWS;

    for (int idx = 0; idx < until; idx++) {
        vga_buffer[idx] = vga_buffer[idx + offset];
    }
    for (int idx = until; idx < last; idx++) {
        vga_buffer[idx] = terminal_char(' ');
    }
}

void terminal_log(const char *str, const char *level, uint8_t color) {
    uint8_t previous_fg = fg_color;
    terminal_set_fg_color(color);

    terminal_print("[kernel ");
    terminal_print(level);
    terminal_print(" - 0]  ");
    for (size_t i = 0; str[i] != '\0' && str[i] != '\n'; i++) terminal_put_c(str[i]);
    terminal_put_c('\n');

    terminal_set_fg_color(previous_fg);
}

void terminal_log_info(const char *str) {
    terminal_log(str, "inf", COL_WHITE);
}

void terminal_log_debug(const char *str) {
    terminal_log(str, "dbg", COL_D_GRAY);
}

void terminal_log_warning(const char *str) {
    terminal_log(str, "war", COL_YELLOW);
}

void terminal_log_error(const char *str) {
    terminal_log(str, "err", COL_RED);
}

void terminal_log_critical(const char *str) {
    uint8_t previous_bg = bg_color;
    terminal_set_bg_color(COL_RED);
    terminal_log(str, "crt", COL_YELLOW);
    terminal_set_bg_color(previous_bg);
}


void terminal_set_fg_color(uint8_t color) {
    fg_color = 0xF & color;
}

void terminal_set_bg_color(uint8_t color) {
    bg_color = 0xF & color;
}

void terminal_set_blink(uint8_t blink) {
    blinking = blink != 0 ? 1 : 0;
}