# My Kernel

![](doc/crude.png) ![](doc/lang.png)

The humble (and highly embryonic) beginning of a custom kernel.

It is based on the i386 architecture for now, but is structured to be extended into others in the future.

![](doc/readme_image.png)

## Building

First you need a [Cross compiler](https://en.wikipedia.org/wiki/Cross_compiler#:~:text=A%20cross%20compiler%20is%20a,smartphone%20is%20a%20cross%20compiler.) 
to build it. In a nutshell, download the latest GCC and Binutils, then:

```script
export PREFIX="$HOME/opt/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"
mkdir -p $PREFIX

# Binutils
cd src/
mkdir build-binutils
cd build-binutils
../binutils-2.34.90/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror
make
make install

# GCC
mkdir build-gcc
cd build-gcc
../gcc-10.2.0/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc
```

And put this somewhere:
```script
export PATH="$HOME/opt/cross/bin:$PATH"
```

Then just run the makefile.

## Running on emulator

It can be run on `QEMU` with `make run-i386` 

## Installing for real

After building, a `kernel.iso` image will be created on `./build` that can be used to create a bootable device. It 
would probably need the `legacy mode` on the BIOS to be enabled, since I am using Multiboot.

## Roadmap

- [x] Make it bootable via GRUB
- [x] Use the  VGA buffer to display colored text
- [ ] Setup the Global Descriptor Table and run in protected mode
- [ ] Setup Interrupts
- [ ] Get keyboard input
- [ ] Setup timers
- [ ] Create a basic shell