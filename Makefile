CFLAGS?=-O3 -g
CFLAGS:=$(CFLAGS) -Wall -Wextra -std=gnu17 -ffreestanding
CC:=$(HOME)/opt/cross/bin/i686-elf-gcc
LDFLAGS?=

BUILD_DIR:=build
ARCH_SRC:=kernel/arch
ARCHS:=$(notdir $(wildcard $(ARCH_SRC)/*))

all: $(ARCHS)

$(ARCHS):
	set -e
	mkdir -p $(BUILD_DIR)/$@
	cd $(BUILD_DIR)/$@ && $(CC) $(CFLAGS) $(LDFLAGS) -I../../$(ARCH_SRC)/$@ -c $(addprefix ../../, $(wildcard $(ARCH_SRC)/$@/*.c))
	cd $(BUILD_DIR)/$@ && $(CC) $(CFLAGS) $(LDFLAGS) -masm=att -c $(addprefix ../../, $(wildcard $(ARCH_SRC)/$@/*.S))
	cd $(BUILD_DIR)/$@ && $(CC) $(CFLAGS) $(LDFLAGS) -I../../include/kernel -c $(addprefix ../../, $(wildcard kernel/kernel/*.c))
	$(CC) $(CFLAGS) $(LDFLAGS)  -nostdlib -T kernel/arch/$@/linker.ld build/$@/*.o $(OBJS) -o $(BUILD_DIR)/$@/kernel.bin -lgcc
	rm -rf /tmp/kerneliso
	mkdir -p /tmp/kerneliso/root/boot/grub
	cp build/$@/kernel.bin /tmp/kerneliso/root/boot/kernel
	cp boot/grub.cfg /tmp/kerneliso/root/boot/grub
	cd /tmp/kerneliso
	grub-mkrescue /tmp/kerneliso/root -o build/$@/kernel.iso
	rm -rf /tmp/kerneliso
	rm -rf build/$@/*.o

run-i386: all
	@echo "To quit, type CTRL + ALT + 2 then quit"
	qemu-system-i386 -kernel build/i386/kernel.bin

clean:
	rm -rf build

