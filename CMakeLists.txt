cmake_minimum_required(VERSION 3.15)
project(kernel C)

set(CMAKE_C_STANDARD 11)

add_executable(kernel kernel/arch/i386/boot.S kernel/kernel/kernel.c kernel/arch/i386/term.c kernel/include/kernel/term.h)